<?php

use ChurchRenewal\Diffy\StyleInsensitiveDiff;
use PHPUnit\Framework\TestCase;

final class StyleInsensitiveDiffTest extends TestCase {
    private const NO_CHANGES = '<div class="block p-4 mb-4 text-white rounded italic" style="background-color: green">There aren\'t any changes to highlight</div>';

    public function testHelloWorld(): void {
        $diff = new StyleInsensitiveDiff(
            "<p>Hello world</p>",
            "<p>Hello everyone</p>",
        );

        $this->assertEquals(
            '<p>Hello <del class="diffdel">world</del><ins class="diffins">everyone</ins></p>',
            trim($diff->getDiff()),
        );

        $diff = new StyleInsensitiveDiff(
            "<p><i>Hello world</i></p>",
            "<p>Hello everyone</p>",
        );

        $this->assertEquals(
            '<p>Hello <i><del class="diffdel">world</del></i><ins class="diffins">everyone</ins></p>',
            trim($diff->getDiff()),
        );
    }

    public function testNoChanges(): void {
        $diff = new StyleInsensitiveDiff(
            "<p>Hello world</p>",
            "<p>Hello world</p>",
        );

        $this->assertEquals(
            self::NO_CHANGES . ' <p>Hello world</p>',
            trim($diff->getDiff()),
        );

        $diff = new StyleInsensitiveDiff(
            "<p>Hell&#x6f; world</p>",
            "<p>Hello world</p>",
        );

        $this->assertEquals(
            self::NO_CHANGES . ' <p>Hello world</p>',
            trim($diff->getDiff()),
        );

        $diff = new StyleInsensitiveDiff(
            "<p>Hello world</p>",
            "<div>Hello world</div>",
        );

        $this->assertEquals(
            self::NO_CHANGES . " <div>Hello world</div>",
            trim($diff->getDiff()),
        );
    }

    public function testNestedElements(): void {
        $diff = new StyleInsensitiveDiff(
            "<p>Hello and <i>welcome back <b>to the show</p>",
            "<p>I'm <u>your</u> host, <a href=\"https://example.com/john-smith\">John Smith</a>, and today we'll be<sup>1</p>",
        );

        $this->assertEquals(
            '<p><del class="diffdel">Hello</del><ins class="diffins">I\'m</ins> <del class="diffdel">and</del><u><ins class="diffins">your</ins></u><ins class="diffins"> host,</ins> <i><del class="diffdel">welcome</del></i><a href="https://example.com/john-smith"><ins class="diffins">John</ins> </a><i><del class="diffdel">back</del></i><a><ins class="diffins">Smith</ins></a><ins class="diffins">, and</ins> <i><b><del class="diffdel">to</del></b></i><ins class="diffins">today</ins> <i><b><del class="diffdel">the</del></b></i><ins class="diffins">we\'ll</ins> <i><b><del class="diffdel">show</del></b></i><ins class="diffins">be</ins><sup><ins class="diffins">1</ins></sup></p>',
            trim($diff->getDiff()),
        );
    }

    public function testHtmlEntities(): void {
        $diff = new StyleInsensitiveDiff(
            "<p attr1 attr2>Hell&#x6f; world</p>",
            "<p attr2 attr1>Hell&#x6f; there</p>",
        );

        $this->assertEquals(
            '<p attr2="" attr1="">Hello <del class="diffdel">world</del><ins class="diffins">there</ins></p>',
            trim($diff->getDiff()),
        );
    }

    public function testOlElementType(): void {
        $diff = new StyleInsensitiveDiff(
            "<ul><li>Hello world</li></ul>",
            "<ol type=a><li>Hello there</li></ol>",
        );

        $this->assertEquals(
            '<ol type="a"> <li>Hello </li></ol><ul><li><del class="diffdel">world</del></li></ul><ol type=a><li><ins class="diffins">there</ins></li> </ol>',
            trim($diff->getDiff()),
        );

        $diff = new StyleInsensitiveDiff(
            "<ul><li>Hello world</li></ul>",
            "<ol type=1><li>Hello there</li></ol>",
        );

        $this->assertEquals(
            '<ol type="1"> <li>Hello </li></ol><ul><li><del class="diffdel">world</del></li></ul><ol type=1><li><ins class="diffins">there</ins></li> </ol>',
            trim($diff->getDiff()),
        );
    }

    public function testRearrangingElementAttributes(): void {
        $diff = new StyleInsensitiveDiff(
            "<p attr1 attr2>Hello world</p>",
            "<p attr2 attr1>Hello there</p>",
        );

        $this->assertEquals(
            '<p attr2="" attr1="">Hello <del class="diffdel">world</del><ins class="diffins">there</ins></p>',
            trim($diff->getDiff()),
        );

        $diff = new StyleInsensitiveDiff(
            "<p attr1=a attr2='b'>Hello world</p>",
            "<p attr2=b attr1='a'>Hello there</p>",
        );

        $this->assertEquals(
            '<p attr2="b" attr1="a">Hello <del class="diffdel">world</del><ins class="diffins">there</ins></p>',
            trim($diff->getDiff()),
        );
    }

    public function testRearrangingElementClasses(): void {
        $diff = new StyleInsensitiveDiff(
            '<p class="strong manly">Hello world</p>',
            '<p class="manly strong">Hello there</p>',
        );

        $this->assertEquals(
            '<p class="manly strong">Hello <del class="diffdel">world</del><ins class="diffins">there</ins></p>',
            trim($diff->getDiff()),
        );
    }
}
