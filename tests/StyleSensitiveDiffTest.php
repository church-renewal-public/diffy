<?php

use ChurchRenewal\Diffy\StyleSensitiveDiff;
use PHPUnit\Framework\TestCase;

final class StyleSensitiveDiffTest extends TestCase {
    private const NO_CHANGES = '<div class="block p-4 mb-4 text-white rounded" style="background-color: green"><b>There aren\'t any changes to highlight</b><br>Perhaps the only change was publication or the arrangement an element\'s attributes</div>';

    public function testHelloWorld(): void {
        $diff = new StyleSensitiveDiff(
            "<p>Hello world</p>",
            "<p>Hello everyone</p>",
        );

        $this->assertEquals(
            '<p>Hello<del class="diffdel"> world</del><ins class="diffins"> everyone</ins></p>',
            trim($diff->getDiff()),
        );

        $diff = new StyleSensitiveDiff(
            "<p><i>Hello world</i></p>",
            "<p>Hello everyone</p>",
        );

        $this->assertEquals(
            '<p><i><del class="diffdel">Hello world</del></i><ins class="diffins">Hello everyone</ins></p>',
            trim($diff->getDiff()),
        );
    }

    public function testNoChanges(): void {
        $diff = new StyleSensitiveDiff(
            "<p>Hello world</p>",
            "<p>Hello world</p>",
        );

        $this->assertEquals(
            self::NO_CHANGES . ' <p>Hello world</p>',
            trim($diff->getDiff()),
        );
    }

    public function testNestedElements(): void {
        $diff = new StyleSensitiveDiff(
            "<p>Hello and <i>welcome back <b>to the show</p>",
            "<p>I'm <u>your</u> host, <a href=\"https://example.com/john-smith\">John Smith</a>, and today we'll be<sup>1</p>",
        );

        $this->assertEquals(
            '<p><del class="diffdel">Hello and </del><i><del class="diffdel">welcome back </del><b><del class="diffdel">to the show</del></b></i><ins class="diffins">I\'m </ins><u><ins class="diffins">your</ins></u><ins class="diffins"> host, </ins><a href="https://example.com/john-smith"><ins class="diffins">John Smith</ins></a><ins class="diffins">, and today we\'ll be</ins><sup><ins class="diffins">1</ins></sup></p>',
            trim($diff->getDiff()),
        );
    }

    public function testChangeElementName(): void {
        $diff = new StyleSensitiveDiff(
            "<p>Hello world</p>",
            "<div>Hello world</div>",
        );

        $this->assertEquals(
            '<p><del class="diffdel">Hello world</del></p><div><ins class="diffins">Hello world</ins></div>',
            trim($diff->getDiff()),
        );
    }

    public function testChangingElementAttributeOrder(): void {
        $diff = new StyleSensitiveDiff(
            "<p attr1 attr2>Hello world</p>",
            "<p attr2 attr1>Hello world</p>",
        );

        $this->assertEquals(
            self::NO_CHANGES . ' <p attr2="" attr1="">Hello world</p>',
            trim($diff->getDiff()),
        );

        $diff = new StyleSensitiveDiff(
            "<p attr1=a attr2='b'>Hello world</p>",
            "<p attr2=b attr1='a'>Hello world</p>",
        );

        $this->assertEquals(
            self::NO_CHANGES . ' <p attr2="b" attr1="a">Hello world</p>',
            trim($diff->getDiff()),
        );
    }

    public function testChangingElementClassOrder(): void {
        $diff = new StyleSensitiveDiff(
            '<p class="strong manly">Hello world</p>',
            '<p class="manly strong">Hello world</p>',
        );

        $this->assertEquals(
            self::NO_CHANGES . ' <p class="manly strong">Hello world</p>',
            trim($diff->getDiff()),
        );
    }
}
