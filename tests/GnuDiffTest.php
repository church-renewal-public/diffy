<?php

use ChurchRenewal\Diffy\GnuDiff;
use PHPUnit\Framework\TestCase;

final class GnuDiffTest extends TestCase {
    public function testHelloWorld(): void {
        $this->assertEquals(
            [
                ["start" => 1, "end" => 2, "delta" => -1],
                ["start" => 1, "end" => 2, "delta" => 1]
            ],
            GnuDiff::getDiff(
                ["Hello", "world"],
                ["Hello", "everyone"],
            ),
        );
    }

    public function testNoChanges(): void {
        $this->assertEquals(
            [],
            GnuDiff::getDiff(
                ["Hello", "world"],
                ["Hello", "world"],
            ),
        );
    }

    public function testEmptyOldAndEmptyNew(): void {
        $this->assertEquals(
            [],
            GnuDiff::getDiff(
                [],
                [],
            ),
        );
    }

    public function testEmptyOld(): void {
        $this->assertEquals(
            [["start" => 0, "end" => 1, "delta" => 1]],
            GnuDiff::getDiff(
                [],
                ["Hello"],
            ),
        );
    }

    public function testEmptyNew(): void {
        $this->assertEquals(
            [["start" => 0, "end" => 1, "delta" => -1]],
            GnuDiff::getDiff(
                ["Hello"],
                [],
            ),
        );
    }

    public function testLotsOfChanges(): void {
        $this->assertEquals(
            [
                ["start" => 0, "end" => 1, "delta" => -1],
                ["start" => 0, "end" => 5, "delta" => 5],
                ["start" => 2, "end" => 7, "delta" => -5],
                ["start" => 6, "end" => 9, "delta" => 3]
            ],
            GnuDiff::getDiff(
                ["Hello", "and", "welcome", "back", "to", "the", "show."],
                ["I'm", "your", "host,", "John", "Smith,", "and", "today", "we'll", "be"],
            ),
        );
    }
}
