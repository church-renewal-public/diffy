<?php

namespace ChurchRenewal\Diffy;

interface _HtmlDiffForHumansInterface {
    public function getDiff(): string;
}

abstract class HtmlDiffForHumans implements _HtmlDiffForHumansInterface {
    // what characters should count as a word separator?
    public const WORD_SEPARATORS = [' ', ',', '.', '/', '(', ')', '&', ';', ':', '-', '+', '–', '—']; // &ndash;, &mdash;

    public const INSERTION_ELEMENT_NAME = 'ins';
    public const INSERTION_ELEMENT_CLASS_ATTR = 'diffins';
    public const DELETION_ELEMENT_NAME = 'del';
    public const DELETION_ELEMENT_CLASS_ATTR = 'diffdel';

    protected const HTML_PARSE_STATE_TEXT = 0;
    protected const HTML_PARSE_STATE_ENTITY = 1;
    protected const HTML_PARSE_STATE_TAG = 2;
    protected const HTML_PARSE_STATE_TAG_CLOSING = 3;

    protected readonly string $delTagStart;
    protected readonly string $delTagEnd;
    protected readonly string $insTagStart;
    protected readonly string $insTagEnd;

    public function __construct(
        protected string $old,
        protected string $new,
    ) {
        $this->old = _TidyHtml5::tidy($this->old);
        $this->new = _TidyHtml5::tidy($this->new);

        $this->delTagStart = '<' . self::DELETION_ELEMENT_NAME . ' class="' . self::DELETION_ELEMENT_CLASS_ATTR . '">';
        $this->delTagEnd = '</' . self::DELETION_ELEMENT_NAME . '>';
        $this->insTagStart = '<' . self::INSERTION_ELEMENT_NAME . ' class="' . self::INSERTION_ELEMENT_CLASS_ATTR . '">';
        $this->insTagEnd = '</' . self::INSERTION_ELEMENT_NAME . '>';
    }

    protected static function normalizeWhitespace(string $text, bool $collapseContiguousWhitespace = false): string {
        return $collapseContiguousWhitespace ? preg_replace('/[\s\n]+/u', ' ', $text) : preg_replace('/[\s\n]/u', ' ', $text);
    }
}
