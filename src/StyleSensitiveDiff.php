<?php

namespace ChurchRenewal\Diffy;

final class StyleSensitiveDiff extends HtmlDiffForHumans {
    private const DIN_DELETE = "\x11"; // ASCII DC1, originally designed for device control, but isn't used by anything today, especially in HTML
    private const DIN_INSERT = "\x12"; // ASCII DC2, originally designed for device control, but isn't used by anything today, especially in HTML
    private const DIN_NONE   = "\x13"; // ASCII DC3, originally designed for device control, but isn't used by anything today, especially in HTML

    public function getDiff(): string {
        $oldLines = self::sortHtmlTagAttributes(explode("\n", self::putHtmlTagsOnTheirOwnLines($this->old)));
        $newLines = self::sortHtmlTagAttributes(explode("\n", self::putHtmlTagsOnTheirOwnLines($this->new)));

        $diffs = GnuDiff::getDiff($oldLines, $newLines);

        $diffs = array_map(function (array $diff) use ($oldLines, $newLines): array {
            $diff['is_insertion'] = $diff['delta'] > 0;
            $diff['length'] = abs($diff['delta']);
            unset($diff['delta']);

            return $diff;
        }, $diffs);

        if (count($diffs) == 0) {
            return <<<HTML
                <div class="block p-4 mb-4 text-white rounded" style="background-color: green"><b>There aren't any changes to highlight</b><br>Perhaps the only change was publication or the arrangement an element's attributes</div>
            HTML . $this->new;
        }

        $deletedDiffs = array_filter($diffs, fn ($a) => !$a['is_insertion']);
        usort($deletedDiffs, fn ($a, $b) => $a['start'] <=> $b['start']);
        $deletedDiffs = array_values($deletedDiffs);

        $insertedDiffs = array_filter($diffs, fn ($a) => $a['is_insertion']);
        usort($insertedDiffs, fn ($a, $b) => $a['start'] <=> $b['start']);
        $insertedDiffs = array_values($insertedDiffs);

        $lines = [];

        // merge the two sets of lines together -->>
        $deletedCounter = $insertedCounter = 0;
        while (!empty($deletedDiffs) || !empty($insertedDiffs)) {
            if (!empty($deletedDiffs) && $deletedDiffs[0]['start'] <= $deletedCounter) {
                array_push($lines, ...array_map(fn ($a) => self::DIN_DELETE . $a, array_splice($oldLines, 0, $deletedDiffs[0]['length'])));
                $oldLines = array_values($oldLines);
                $deletedCounter += $deletedDiffs[0]['length'];
                array_shift($deletedDiffs);
            }
            if (!empty($insertedDiffs) && $insertedDiffs[0]['start'] <= $insertedCounter) {
                array_push($lines, ...array_map(fn ($a) => self::DIN_INSERT . $a, array_splice($newLines, 0, $insertedDiffs[0]['length'])));
                $newLines = array_values($newLines);
                $insertedCounter += $insertedDiffs[0]['length'];
                array_shift($insertedDiffs);
            }
            $lines[] = self::DIN_NONE . array_shift($oldLines);
            array_shift($newLines);
            $deletedCounter++;
            $insertedCounter++;
        }
        array_push($lines, ...array_map(fn ($a) => self::DIN_NONE . $a, $newLines));
        // <<-- merge the two sets of lines together

        // header('Content-Type: text/plain');
        // die(str_replace([self::DIN_DELETE, self::DIN_INSERT, self::DIN_NONE], ['-', '+', ' '], implode("\n", $lines)));

        for ($lineNumber = 0; $lineNumber < count($lines); $lineNumber++) {
            $line = &$lines[$lineNumber];
            $nextLine = $lines[$lineNumber + 1] ?? '';

            // if $line is del and $line is tag and $nextLine is ins and $nextLine is tag {
            if ($line[0] == self::DIN_DELETE && (trim(substr($line, 1))[0] ?? '') == '<' && $nextLine[0] == self::DIN_INSERT && (trim(substr($nextLine, 1))[0] ?? '') == '<') {
                // $delLines = all lines from $line through to the next line with the same indentation as $line that aren't `ins`es.
                $delLines = (function () use ($lines, $lineNumber): array { // so we can mess with $lineNumber without clobbering it
                    $result = [$lines[$lineNumber]];
                    $origLine = $lines[$lineNumber];
                    $origIndentation = self::getIndentation($origLine);
                    $indentation = -1; // not the same as $origIndentation
                    while ($indentation != $origIndentation) {
                        $lineNumber++;
                        $line = $lines[$lineNumber];
                        if ($line[0] != self::DIN_INSERT) {
                            $result[] = $line;
                            $indentation = self::getIndentation($line);
                        }
                    }
                    return $result;
                })();

                // $insLines = (likewise, but other d/i)
                $insLines = (function () use ($lines, $lineNumber): array { // so we can mess with $lineNumber without clobbering it
                    $lineNumber++; // for some reason
                    $result = [$lines[$lineNumber]];
                    $origLine = $lines[$lineNumber];
                    $origIndentation = self::getIndentation($origLine);
                    $indentation = -1; // not the same as $origIndentation
                    while ($indentation != $origIndentation) {
                        $lineNumber++;
                        $line = $lines[$lineNumber];
                        if ($line[0] != self::DIN_DELETE) {
                            $result[] = $line;
                            $indentation = self::getIndentation($line);
                        }
                    }
                    return $result;
                })();

                // remove the d/i/n indicator from all lines in $delLines and $insLines
                $delLines = array_map(fn (string $line) => substr($line, 1), $delLines);
                $insLines = array_map(fn (string $line) => substr($line, 1), $insLines);

                // surround every text line in $delLines with del tags
                $delLines = array_map(function (string $line): string {
                    if ((trim($line)[0] ?? '') == '<') {
                        return $line;
                    } else {
                        return str_repeat("\t", self::getIndentation($line)) . $this->delTagStart . ltrim($line, "\t") . $this->delTagEnd;
                    }
                }, $delLines);

                // likewise with $insLines, but with other d/i
                $insLines = array_map(function (string $line): string {
                    if ((trim($line)[0] ?? '') == '<') {
                        return $line;
                    } else {
                        return str_repeat("\t", self::getIndentation($line)) . $this->insTagStart . ltrim($line, "\t") . $this->insTagEnd;
                    }
                }, $insLines);

                // replace all lines from $line though to (the next line with the same indentation as $line that is the opposite d/i or n) with $delLines concat'd with $insLines
                $origLineNumber = $lineNumber;
                $origIndentation = self::getIndentation($line);
                $lineNumber += 2;
                while (isset($lines[$lineNumber]) && !(self::getIndentation($lines[$lineNumber]) == $origIndentation && ($lines[$lineNumber][0] == self::DIN_INSERT || $lines[$lineNumber][0] == self::DIN_NONE))) {
                    $lineNumber++;
                }
                array_splice($lines, $origLineNumber, $lineNumber - $origLineNumber + 1, [...$delLines, ...$insLines]);

                // advance the line pointer to after all of that ^^^
                $lineNumber = $origLineNumber + count($delLines) + count($insLines) - 1;
                continue;

                // dd($delLines, $insLines, );
            } else if ($line[0] == self::DIN_DELETE && (trim(substr($line, 1))[0] ?? '') != '<') {
                $indentation = self::getIndentation($line);
                $line = substr($line, 1); // remove the del/ins/non indicator
                $line = ltrim($line, "\t");

                if ($nextLine[0] == self::DIN_INSERT && (trim(substr($nextLine, 1))[0] ?? '') != '<' && self::getIndentation($nextLine) == $indentation) {
                    $line = str_repeat("\t", $indentation) . ((string) $this->getTextDiff($line, ltrim(substr($nextLine, 1), "\t"))); // diff the line against its next neighbour
                    unset($lines[$lineNumber + 1]);
                    $lines = array_values($lines);
                } else {
                    $line = str_repeat("\t", $indentation) . $this->delTagStart . $line . $this->delTagEnd; // surround the text with del tag
                }
            } else if ($line[0] == self::DIN_INSERT && (trim(substr($line, 1))[0] ?? '') != '<') {
                $line = substr($line, 1); // remove the del/ins/non indicator
                preg_match('#^\t*#', $line, $matches);
                $indentation = $matches[0];
                $line = ltrim($line, "\t");
                $line = $indentation . $this->insTagStart . $line . $this->insTagEnd; // surround the text with ins tag
            } else {
                $line = substr($line, 1); // remove the del/ins/non indicator
            }
        }
        unset($line); // without this, the last line will be repeated for some reason

        if (!array_is_list($lines)) throw new \Exception('$lines is not a list');

        foreach ($lines as $index => $line) { // this should be impossible and, therefore, impossible to test
            if (isset($line[0]) && $line[0] == self::DIN_DELETE) {
                dump($lines);
                throw new \Exception('Missed a spot! (deletion at line ' . $index . ')');
            } else if (isset($line[0]) && $line[0] == self::DIN_INSERT) {
                dump($lines);
                throw new \Exception('Missed a spot! (insertion at line ' . $index . ')');
            } else if (isset($line[0]) && $line[0] == self::DIN_NONE) {
                dump($lines);
                throw new \Exception('Missed a spot! (no_operation at line ' . $index . ')');
            }
        }

        $html = str_replace("\t", '', implode($lines));

        $html = preg_replace('/' . preg_quote($this->insTagStart, '/') . '\s+' . preg_quote($this->insTagEnd, '/') . '/u', '', $html); // remove inserts with only whitespace
        $html = preg_replace('/' . preg_quote($this->delTagStart, '/') . '\s+' . preg_quote($this->delTagEnd, '/') . '/u', '', $html); // remove deletes with only whitespace

        return $html;
    }

    private function getTextDiff(string $oldText, string $newText): string {
        $oldWords = self::textToWords($oldText);
        $newWords = self::textToWords($newText);
        $diffs = GnuDiff::getDiff($oldWords, $newWords);
        $diffs = array_map(function (array $diff) use ($oldWords, $newWords): array {
            $diff['is_insertion'] = $diff['delta'] > 0;
            $diff['length'] = abs($diff['delta']);
            unset($diff['delta']);

            return $diff;
        }, $diffs);

        $deletedDiffs = array_filter($diffs, fn ($a) => !$a['is_insertion']);
        usort($deletedDiffs, fn ($a, $b) => $a['start'] <=> $b['start']);
        $deletedDiffs = array_values($deletedDiffs);

        $insertedDiffs = array_filter($diffs, fn ($a) => $a['is_insertion']);
        usort($insertedDiffs, fn ($a, $b) => $a['start'] <=> $b['start']);
        $insertedDiffs = array_values($insertedDiffs);

        $words = [];

        // merge the two sets of words together -->>
        $deletedCounter = $insertedCounter = 0;
        while (!empty($deletedDiffs) || !empty($insertedDiffs)) {
            if (!empty($deletedDiffs) && $deletedDiffs[0]['start'] <= $deletedCounter) {
                array_push($words, ...array_map(fn ($a) => self::DIN_DELETE . $a, array_splice($oldWords, 0, $deletedDiffs[0]['length'])));
                $oldWords = array_values($oldWords);
                $deletedCounter += $deletedDiffs[0]['length'];
                array_shift($deletedDiffs);
            }
            if (!empty($insertedDiffs) && $insertedDiffs[0]['start'] <= $insertedCounter) {
                array_push($words, ...array_map(fn ($a) => self::DIN_INSERT . $a, array_splice($newWords, 0, $insertedDiffs[0]['length'])));
                $newWords = array_values($newWords);
                $insertedCounter += $insertedDiffs[0]['length'];
                array_shift($insertedDiffs);
            }
            $words[] = self::DIN_NONE . array_shift($oldWords);
            array_shift($newWords);
            $deletedCounter++;
            $insertedCounter++;
        }
        array_push($words, ...array_map(fn ($a) => self::DIN_NONE . $a, $newWords));
        // <<-- merge the two sets of words together

        foreach ($words as &$word) {
            if ($word[0] == self::DIN_DELETE) {
                $word = $this->delTagStart . substr($word, 1) . $this->delTagEnd;
            } else if ($word[0] == self::DIN_INSERT) {
                $word = $this->insTagStart . substr($word, 1) . $this->insTagEnd;
            } else { // self::DIN_NONE
                $word = substr($word, 1);
            }
        }
        unset($word);

        $words = implode($words);
        $words = str_replace([$this->delTagEnd . $this->delTagStart, $this->insTagEnd . $this->insTagStart], ['', ''], $words);

        return $words;
    }

    private static function textToWords(string $text): array {
        $words = [''];
        foreach (mb_str_split($text) as $char) {
            if (in_array($char, self::WORD_SEPARATORS)) {
                $words[] = '';
            }
            $words[count($words) - 1] .= $char;
        }
        return array_values(array_filter($words, 'strlen'));
    }

    private static function getIndentation(string $line): int {
        if (in_array($line[0], [self::DIN_DELETE, self::DIN_INSERT, self::DIN_NONE]))
            $line = substr($line, 1);
        preg_match('#^\t*#', $line, $matches);
        return strlen($matches[0]);
    }

    private static function putHtmlTagsOnTheirOwnLines(string $html): string {
        $html = self::normalizeWhitespace($html, true);
        $html = str_replace('<', "\n<", $html);
        $html = str_replace('>', ">\n", $html);
        $html = explode("\n", $html);
        $html = array_filter($html, 'strlen');
        $depth = 0;
        foreach ($html as &$line) {
            $lineOrig = $line;
            if (str_starts_with($lineOrig, '</')) $depth--;
            $line = str_repeat("\t", $depth) . $line;
            if (str_starts_with($lineOrig, '<') && !str_starts_with($lineOrig, '</')) $depth++;
        }
        $html = implode("\n", $html);
        return $html;
    }

    private static function sortHtmlTagAttributes(array $lines): array {
        foreach ($lines as &$line) {
            if (str_starts_with(trim($line), '<')) { // it's a tag
                unset($matches);
                preg_match_all('/\w+(=([^\'"\s]+|"[^"]+"|\'[^\']+\'))?/mu', trim($line), $matches);

                foreach ($matches[0] as &$match) {
                    if (!str_contains($match, '=')) { // e.g. checked
                        continue;
                    }

                    if (!str_contains($match, '="') && !str_contains($match, "='")) { // e.g. target=_blank
                        $match = str_replace('=', '="', $match) . '"'; // e.g. target="_blank"
                    } else if (str_contains($match, "='")) { // e.g. target='_blank'
                        $match = str_replace('"', '&quot;', $match); // e.g. data-name="Robert &quot;Bob&quot; Smith"
                    }

                    $attributeName = strtolower(substr($match, 0, strpos($match, '=')));
                    $attributeTheOtherPart = substr($match, strpos($match, '=') + 1);

                    if ($attributeName == 'class') {
                        $classes = explode(' ', substr($attributeTheOtherPart, 1, -1));
                        sort($classes);
                        $attributeTheOtherPart = '"' . implode(' ', $classes) . '"';
                    }

                    $match = "$attributeName=$attributeTheOtherPart";
                }

                $tagName = $matches[0][0];
                $attributes = array_slice($matches[0], 1);
                sort($attributes);

                unset($linePrefix);
                preg_match('#^\s*</?#u', $line, $linePrefix);
                unset($lineSuffix);
                preg_match('#(/\s*)?>\s*$#u', $line, $lineSuffix);
                $line = $linePrefix[0] . trim($tagName . ' ' . implode(' ', $attributes)) . $lineSuffix[0];
            }
        }

        return $lines;
    }
}
