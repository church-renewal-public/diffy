<?php

namespace ChurchRenewal\Diffy;

final class StyleInsensitiveDiff extends HtmlDiffForHumans {
    /**
     * WARNING: This class was not written to be easily maintainable. It was made to
     *          be fast. Really fast. "Are you sure that's the right answer?" fast.
     */

    private array $oldWords;
    private array $newWords;
    private array $diffs;
    private int $diffPointer;
    private int $wordPointer;
    private int $wordCharPointer;
    private array $wordSeparatorsAssoc;
    private array $delTagStartExploded;
    private array $delTagEndExploded;

    public function getDiff(): string {
        ini_set('memory_limit', '256M');
        $this->wordSeparatorsAssoc = array_flip(self::WORD_SEPARATORS);
        $this->delTagStartExploded = mb_str_split($this->delTagStart);
        $this->delTagEndExploded = mb_str_split($this->delTagEnd);

        /**
         * The general operations performed are:
         * 1. Get a list of words from the old content
         * 2. Get a list of words from the new content
         * 3. Get a list of differences between the two from Diffy, ignoring whitespace-only changes
         * 4. Create an html from the new content; let's call it the diff DOM
         * 5. Surround text that Diffy said was inserted with <ins> tags
         * 6. Insert <del> tags with the text (including styling) that Diffy said was deleted
         */

        $this->oldWords = $this->htmlToWords($this->old); // 1. Get a list of words from the old content
        $this->newWords = $this->htmlToWords($this->new); // 2. Get a list of words from the new content

        // 3. Get a list of differences between the two from Diffy, ignoring whitespace-only changes
        $this->diffs = GnuDiff::getDiff($this->oldWords, $this->newWords);
        usort($this->diffs, fn ($a, $b) => $a['start'] <=> $b['start']);
        $this->diffs = array_values(array_filter($this->diffs, function ($diff): bool {
            if ($diff['delta'] > 0) { // is insertion
                $words = &$this->newWords;
            } else {
                $words = &$this->oldWords;
            }

            if (trim(implode(array_slice($words, $diff['start'], abs($diff['delta'])))) == '') return false;
            return true;
        }));
        $this->diffs = array_map(fn ($a) => ['start' => $a['start'], 'end' => $a['end'], 'length' => abs($a['delta']), 'is_insertion' => $a['delta'] > 0], $this->diffs);

        if (count($this->diffs) == 0) {
            return <<<HTML
                <div class="block p-4 mb-4 text-white rounded italic" style="background-color: green">There aren't any changes to highlight</div>
            HTML . $this->new;
        }

        $diff = $this->new; // 4. Create an html from the new content; let's call it the diff DOM
        $diff = $this->highlightInsertions($diff); // 5. Surround text that Diffy said was inserted with <ins> tags
        $diff = $this->highlightDeletions($diff); // 6. Insert <del> tags with the text (including styling) that Diffy said was deleted

        return $diff;
    }

    private static function toCharsAndStyle(string $html): array {
        $html = mb_str_split((string) $html);
        $charsAndStyle = [];
        $state = self::HTML_PARSE_STATE_TEXT;
        $stylePath = [];
        $tag = '';
        $htmlEntity = '';

        for ($htmlPointer = 0; true; $htmlPointer++) {
            $htmlChar = $html[$htmlPointer] ?? null;
            if ($htmlChar === null) break;
            $textChar = '';

            switch ($state) {
                case (self::HTML_PARSE_STATE_TEXT):
                    if ($htmlChar == '<') {
                        $tag = '';
                        $state = self::HTML_PARSE_STATE_TAG;
                    } else if ($htmlChar == '&') {
                        $htmlEntity = $htmlChar;
                        $state = self::HTML_PARSE_STATE_ENTITY;
                    } else {
                        $textChar = $htmlChar;
                    }
                    break;
                case (self::HTML_PARSE_STATE_ENTITY):
                    $htmlEntity .= $htmlChar;
                    if ($htmlChar == ';') {
                        $textChar = html_entity_decode($htmlEntity);
                        $state = self::HTML_PARSE_STATE_TEXT;
                    }
                    break;
                case (self::HTML_PARSE_STATE_TAG):
                    if ($htmlChar == '/' && $tag == '') {
                        $state = self::HTML_PARSE_STATE_TAG_CLOSING;
                    } else if ($htmlChar == '>') {
                        $tag = explode(' ', $tag);
                        $styleComponent = $tag[0];

                        if (strcasecmp($styleComponent, 'ol') == 0) {
                            foreach ($tag as $a) {
                                if (str_starts_with($a, 'type=')) {
                                    $type = substr($a, 5); // strlen("type=")
                                    $type = str_replace(['"', '\''], ['', ''], $type);
                                    break;
                                }
                            }
                            $type ??= '1';
                            $styleComponent .= " type=$type"; // 'ol type=1', 'ol type=a'
                        }
                        $stylePath[] = $styleComponent;
                        $tag = '';
                        $state = self::HTML_PARSE_STATE_TEXT;
                    } else {
                        $tag .= $htmlChar;
                    }
                    break;
                case (self::HTML_PARSE_STATE_TAG_CLOSING):
                    if ($htmlChar == '>') {
                        array_pop($stylePath);
                        $state = self::HTML_PARSE_STATE_TEXT;
                    }
                    break;
            }

            if ($textChar != '' && $state == self::HTML_PARSE_STATE_TEXT) {
                $charsAndStyle[] = [$textChar, $stylePath];
            }
        }

        return $charsAndStyle;
    }

    private function groupCharsAndStylesByWord(array $charsAndStyles, array $words): array {
        array_unshift($charsAndStyles, null); // because next() advances the pointer *then* returns the element's value
        reset($charsAndStyles); // move the pointer to the beginning
        $groupedByWord = [[]];

        foreach ($words as $word) {
            $lastCharsAndStylesGroupedByWord = &$groupedByWord[count($groupedByWord) - 1];
            $limit = mb_strlen($word);
            for ($i = 0; $i < $limit; $i++) {
                $lastCharsAndStylesGroupedByWord[] = next($charsAndStyles);
            }
            $groupedByWord[] = [];
        }

        if (empty($groupedByWord[count($groupedByWord) - 1])) array_pop($groupedByWord);

        return $groupedByWord;
    }

    private function htmlToWords(string $html): array {
        $html = mb_str_split((string) $html);
        $words = [];
        $word = '';
        $state = self::HTML_PARSE_STATE_TEXT;
        $htmlEntity = '';

        for ($htmlPointer = 0; true; $htmlPointer++) {
            $htmlChar = $html[$htmlPointer] ?? null;
            if ($htmlChar === null) break;
            $textChar = '';

            switch ($state) {
                case (self::HTML_PARSE_STATE_TEXT):
                    if ($htmlChar == '<') {
                        $state = self::HTML_PARSE_STATE_TAG;
                    } else if ($htmlChar == '&') {
                        $htmlEntity = $htmlChar;
                        $state = self::HTML_PARSE_STATE_ENTITY;
                    } else {
                        $textChar = $htmlChar;
                    }
                    break;
                case (self::HTML_PARSE_STATE_ENTITY):
                    $htmlEntity .= $htmlChar;
                    if ($htmlChar == ';') {
                        $textChar = html_entity_decode($htmlEntity);
                        $state = self::HTML_PARSE_STATE_TEXT;
                    }
                    break;
                case (self::HTML_PARSE_STATE_TAG):
                    if ($htmlChar == '/') {
                        $state = self::HTML_PARSE_STATE_TAG_CLOSING;
                    } else if ($htmlChar == '>') {
                        $state = self::HTML_PARSE_STATE_TEXT;
                    }
                    break;
                case (self::HTML_PARSE_STATE_TAG_CLOSING):
                    if ($htmlChar == '>') $state = self::HTML_PARSE_STATE_TEXT;
                    break;
            }

            if ($state == self::HTML_PARSE_STATE_TEXT) {
                if ($word != '' && isset($this->wordSeparatorsAssoc[$textChar])) {
                    $words[] = $word;
                    $word = '';
                }

                $word .= $textChar;

                if ($word != '' && isset($this->wordSeparatorsAssoc[$textChar])) {
                    $words[] = $word;
                    $word = '';
                }
            }
        }

        return $words;
    }

    /**
     * @return bool whether or not $wordPointer was incremented
     */
    private function incrementWordCharPointer(array &$words): bool {
        if (mb_strlen($words[$this->wordPointer]) > $this->wordCharPointer + 1) {
            $this->wordCharPointer++;
            return false;
        } else {
            $this->wordCharPointer = 0;
            $this->wordPointer++;
            return true;
        }
    }

    private function highlightInsertions(string $html): string {
        $this->diffPointer = $this->wordPointer = $this->wordCharPointer = 0;
        if (!$this->putTheDiffPointerWhereWeExpect(true)) return $html;
        $newHtml = '';
        $state = self::HTML_PARSE_STATE_TEXT;
        $htmlEntity = '';

        foreach (mb_str_split((string) $html) as $index => $htmlChar) {
            $textChar = '';
            $targetChar = '';

            switch ($state) {
                case (self::HTML_PARSE_STATE_TEXT):
                    if ($htmlChar == '<') {
                        $state = self::HTML_PARSE_STATE_TAG;
                    } else if ($htmlChar == '&') {
                        $htmlEntity = $htmlChar;
                        $state = self::HTML_PARSE_STATE_ENTITY;
                    } else {
                        if (!isset($this->newWords[$this->wordPointer])) {
                            die('out of words');
                            break (2);
                        };
                        $textChar = $htmlChar;
                    }
                    break;
                case (self::HTML_PARSE_STATE_ENTITY):
                    $htmlEntity .= $htmlChar;
                    if ($htmlChar == ';') {
                        $textChar = html_entity_decode($htmlEntity);
                        $state = self::HTML_PARSE_STATE_TEXT;
                    }
                    break;
                case (self::HTML_PARSE_STATE_TAG):
                    if ($htmlChar == '/') {
                        $state = self::HTML_PARSE_STATE_TAG_CLOSING;
                    } else if ($htmlChar == '>') {
                        $state = self::HTML_PARSE_STATE_TEXT;
                    }
                    break;
                case (self::HTML_PARSE_STATE_TAG_CLOSING):
                    if ($htmlChar == '>') {
                        $state = self::HTML_PARSE_STATE_TEXT;
                    }
                    break;
            }

            $targetChar = mb_substr($this->newWords[$this->wordPointer], $this->wordCharPointer, 1);
            $needToBreak = false;

            if ($state == self::HTML_PARSE_STATE_TEXT) {
                if ($textChar == $targetChar) {
                    if ($this->diffs[$this->diffPointer]['start'] <= $this->wordPointer) {
                        $htmlChar = $this->insTagStart . $htmlChar . $this->insTagEnd;
                    }

                    if ($this->incrementWordCharPointer($this->newWords) && !$this->putTheDiffPointerWhereWeExpect(true)) $needToBreak = true;
                }
            }

            if (!$this->putTheDiffPointerWhereWeExpect(true)) $needToBreak = true;

            $newHtml .= $htmlChar;
            if ($needToBreak) break;
        }

        $newHtml .= mb_substr($html, $index + 1);
        $newHtml = str_replace($this->insTagEnd . $this->insTagStart, '', $newHtml); // consolidate neighbouring insert elements
        $newHtml = preg_replace('#<([a-z0-9]+)[^>]*></\1>#iu', '', $newHtml); // remove empty elements
        $newHtml = preg_replace('/' . preg_quote($this->insTagStart, '/') . '\s+' . preg_quote($this->insTagEnd, '/') . '/u', '', $newHtml); // remove inserts with only whitespace
        $newHtml = preg_replace('#<([a-z0-9]+)[^>]*></\1>#iu', '', $newHtml); // remove empty elements

        return $newHtml;
    }

    private function highlightDeletions(string $html): string {
        $oldCharsWithStyleGroupedByWord = self::groupCharsAndStylesByWord(self::toCharsAndStyle($this->old), $this->oldWords);
        $this->diffPointer = $this->wordPointer = $this->wordCharPointer = 0;
        if (!$this->putTheDiffPointerWhereWeExpect(false)) return $html;
        $html = mb_str_split((string) $html);
        $html = array_reverse($html);
        $state = self::HTML_PARSE_STATE_TEXT;
        $stylePath = [];
        $tag = '';
        $htmlEntity = '';

        $output = [];

        while (true) {
            $htmlChar = array_pop($html);
            if ($htmlChar === null) break;
            $output[] = $htmlChar;
            $textChar = '';
            $targetChar = '';

            switch ($state) {
                case (self::HTML_PARSE_STATE_TEXT):
                    if ($htmlChar == '<') {
                        $tag = '';
                        $state = self::HTML_PARSE_STATE_TAG;
                    } else if ($htmlChar == '&') {
                        $htmlEntity = $htmlChar;
                        $state = self::HTML_PARSE_STATE_ENTITY;
                    } else if (!in_array(self::INSERTION_ELEMENT_NAME, $stylePath)) {
                        if (!isset($this->oldWords[$this->wordPointer])) break (2);
                        $textChar = $htmlChar;

                        $targetChar = mb_substr($this->oldWords[$this->wordPointer], $this->wordCharPointer, 1);
                        if ($textChar == $targetChar) {
                            if ($this->incrementWordCharPointer($this->oldWords)) if (!$this->putTheDiffPointerWhereWeExpect(false)) break (2);
                        }
                    }
                    break;
                case (self::HTML_PARSE_STATE_ENTITY):
                    $htmlEntity .= $htmlChar;
                    if ($htmlChar == ';') {
                        $textChar = html_entity_decode($htmlEntity);
                        $state = self::HTML_PARSE_STATE_TEXT;

                        if (!in_array(self::INSERTION_ELEMENT_NAME, $stylePath)) {
                            if (!isset($this->oldWords[$this->wordPointer])) break (2);

                            $targetChar = mb_substr($this->oldWords[$this->wordPointer], $this->wordCharPointer, 1);
                            if ($textChar == $targetChar) {
                                if ($this->incrementWordCharPointer($this->oldWords)) if (!$this->putTheDiffPointerWhereWeExpect(false)) break (2);
                            }
                        }
                    }
                    break;
                case (self::HTML_PARSE_STATE_TAG):
                    if ($htmlChar == '/' && $tag == '') {
                        $state = self::HTML_PARSE_STATE_TAG_CLOSING;
                    } else if ($htmlChar == '>') {
                        $tag = explode(' ', $tag);
                        $styleComponent = $tag[0];

                        if (strcasecmp($styleComponent, 'ol') == 0) {
                            foreach ($tag as $a) {
                                if (str_starts_with($a, 'type=')) {
                                    $type = substr($a, 5); // strlen("type=")
                                    $type = str_replace(['"', '\''], ['', ''], $type);
                                    break;
                                }
                            }
                            $type ??= '1';
                            $styleComponent .= " type=$type"; // 'ol type=1', 'ol type=a'
                        }
                        $stylePath[] = $styleComponent;
                        $tag = '';
                        $state = self::HTML_PARSE_STATE_TEXT;
                    } else {
                        $tag .= $htmlChar;
                    }
                    break;
                case (self::HTML_PARSE_STATE_TAG_CLOSING):
                    if ($htmlChar == '>') {
                        array_pop($stylePath);
                        $state = self::HTML_PARSE_STATE_TEXT;
                    }
                    break;
            }

            if (!$this->putTheDiffPointerWhereWeExpect(false)) break;

            if (
                $state == self::HTML_PARSE_STATE_TEXT &&
                isset($this->diffs[$this->diffPointer]) &&
                $this->wordPointer >= $this->diffs[$this->diffPointer]['start']
            ) {
                $diff = $this->diffs[$this->diffPointer];
                $deletedCharsWithStyle = array_merge(...array_slice($oldCharsWithStyleGroupedByWord, $diff['start'], $diff['length']));

                foreach ($deletedCharsWithStyle as $deletedCharWithStyle) {
                    while (true) {
                        if (self::areStylePathsEqual($stylePath, $deletedCharWithStyle[1])) {
                            // insert the deleted character like normal

                            array_push($output, ...$this->delTagStartExploded, ...mb_str_split(htmlentities($deletedCharWithStyle[0])), ...$this->delTagEndExploded);
                            $this->incrementWordCharPointer($this->oldWords);
                            break;
                        } else if (self::doesStylePathBeginWith($deletedCharWithStyle[1], $stylePath)) {
                            // insert more styling, then go back to the top of the foreach

                            $oldPath = array_filter(explode("\x1e", self::normalizeStylePath($deletedCharWithStyle[1])), 'strlen');
                            $herePath = array_filter(explode("\x1e", self::normalizeStylePath($stylePath)), 'strlen');
                            $tagName = $oldPath[count($herePath)];

                            $stylePath[] = $tagName;
                            array_push($output, ...str_split("<$tagName>"));
                            array_push($html, ...array_reverse(str_split("</" . explode(' ', $tagName)[0] . ">")));
                        } else {
                            // remove styling, then go back to the top of the foreach

                            $tagName = array_pop($stylePath);
                            array_push($output, ...str_split("</" . explode(' ', $tagName)[0] . ">"));
                            array_push($html, ...array_reverse(str_split("<$tagName>")));
                        }
                    }
                }
            }
        }

        $html = implode(array_merge($output, array_reverse($html)));
        $html = str_replace($this->delTagEnd . $this->delTagStart, '', $html); // consolidate neighbouring delete elements
        $html = self::removeEmptyElements($html);
        $html = preg_replace('/' . preg_quote($this->delTagStart, '/') . '\s+' . preg_quote($this->delTagEnd, '/') . '/u', '', $html); // remove deletes with only whitespace
        $html = self::removeEmptyElements($html);
        $html = preg_replace('#(' . preg_quote($this->delTagEnd, '#') . '\s*)</([a-z0-9]+)[^>]*><\2>(?!' . preg_quote($this->delTagStart, '#') . ')#iu', '$1', $html); // `</del> </p><p>` => `</del>`
        $html = self::removeEmptyElements($html);

        return $html;
    }

    private static function removeEmptyElements(string $html): string {
        while (true) {
            $newHtml = preg_replace('#<([a-z0-9]+)[^>]*></\1>#iu', '', $html);
            if ($newHtml == $html) return $html;
            $html = $newHtml;
        }
    }

    /**
     * You'd think this is a stupid question
     */
    private static function areStylePathsEqual(array|string $a, array|string $b): bool {
        return self::normalizeStylePath($a) == self::normalizeStylePath($b);
    }

    private static function doesStylePathBeginWith(array|string $haystack, array|string $needle): bool {
        return str_starts_with(self::normalizeStylePath($haystack), self::normalizeStylePath($needle));
    }

    private static function normalizeStylePath(array|string $path): string {
        if (is_string($path)) return $path; // $path = explode("\x1e", $path); // ascii record separator
        return implode("\x1e", array_filter($path, 'strlen')); // ascii record separator
    }

    /**
     * @return bool `true` if the pointer (was changed) points to an acceptable diff; `false` if we've ran out of diffs
     */
    private function putTheDiffPointerWhereWeExpect(bool $requireIsInsertionToEqual): bool {
        while (true) {
            if (!isset($this->diffs[$this->diffPointer])) return false;
            if (
                $this->diffs[$this->diffPointer]['end'] > $this->wordPointer &&
                $this->diffs[$this->diffPointer]['is_insertion'] == $requireIsInsertionToEqual
            ) return true;
            $this->diffPointer++;
        }
    }
}
