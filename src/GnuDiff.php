<?php

namespace ChurchRenewal\Diffy;

final class GnuDiff {
    // `diff` exit codes; https://www.gnu.org/software/diffutils/manual/diffutils.html#Invoking-diff
    public const DIFF_NO_DIFF = 0;
    public const DIFF_DIFF = 1;
    public const DIFF_ERROR = 2;

    private function __construct() {
    }

    /**
     * A thin wrapper around GNU `diff`
     * 
     * @param array $oldStrings flat array of strings (strings may not include a "\n")
     * @param array $newStrings flat array of strings (strings may not include a "\n")
     * @return array a flat array of diffs; see below
     *
     * A diff is an associative array with the keys "start", "end", and "delta".  
     * | element | description |
     * | --- | --- |
     * | `delta` | `int` — the count of elements in the input array that are involved in the change. Negative numbers indicate a deletion and that `start` references `$oldStrings`, and positive numbers indicate an insertion and that `start` references `$newStrings`. This will never be zero |
     * | `start` | `int` — index in the input array that the change starts on |
     * | `end` | `int` — `start` + abs(`delta`) |
     */
    public static function getDiff(array $oldStrings, array $newStrings): array {
        $oldStrings = array_map('strval', $oldStrings); // we can only handle single-dimensional arrays
        $newStrings = array_map('strval', $newStrings);
        if (str_contains(implode($newStrings), "\n")) throw new \InvalidArgumentException('$newStrings[] must not contain a "\n".');
        if (str_contains(implode($oldStrings), "\n")) throw new \InvalidArgumentException('$oldStrings[] must not contain a "\n".');

        if (empty($oldStrings) && empty($newStrings)) {
            return []; // no differences
        }

        if (empty($oldStrings)) {
            return [[
                'start' => 0,
                'end' => count($newStrings),
                'delta' => count($newStrings),
            ]];
        }

        if (empty($newStrings)) {
            return [[
                'start' => 0,
                'end' => count($oldStrings),
                'delta' => -count($oldStrings),
            ]];
        }

        $oldFilepath = '/tmp/' . microtime(true) . bin2hex(random_bytes(24));
        $newFilepath = '/tmp/' . microtime(true) . bin2hex(random_bytes(24));

        file_put_contents($oldFilepath, implode("\n", $oldStrings) . "\n");
        file_put_contents($newFilepath, implode("\n", $newStrings) . "\n");

        $diffOutput = [];
        $diffExitCode = null;
        if (!str_starts_with(`diff -v`, 'diff (GNU diffutils) 3.')) throw new \Exception(<<<TXT
            The output of the shell command `diff -v` must begin with "diff (GNU diffutils) 3.".
        TXT);
        exec('diff --unchanged-line-format="" --old-line-format="-,%dn' . "\n" . '" --new-line-format="+,%dn' . "\n" . '" ' . escapeshellarg($oldFilepath) . ' ' . escapeshellarg($newFilepath), $diffOutput, $diffExitCode);

        unlink($oldFilepath);
        unlink($newFilepath);

        if ($diffExitCode == self::DIFF_ERROR) throw new GnuDiffError();
        if ($diffExitCode == self::DIFF_NO_DIFF) return [];

        $diffs = [];
        foreach ($diffOutput as $outputLine) {
            $output = explode(",", $outputLine); // "-,17" // delete char #17
            $charIndex = $output[1] - 1; // `diff` is 1 based; we want 0 based
            $delta = $output[0] == '+' ? 1 : -1;

            if (count($diffs) && $diffs[count($diffs) - 1]['end'] == $charIndex && ($delta < 0 && $diffs[count($diffs) - 1]['delta'] < 0 || $delta > 0 && $diffs[count($diffs) - 1]['delta'] > 0)) {
                $diffs[count($diffs) - 1]['end']++;
                $diffs[count($diffs) - 1]['delta'] += $delta;
            } else {
                $diffs[] = [
                    'start' => $charIndex,
                    'end' => $charIndex + 1,
                    'delta' => $delta,
                ];
            }
        }

        return $diffs;
    }
}
