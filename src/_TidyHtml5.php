<?php

namespace ChurchRenewal\Diffy;

final class _TidyHtml5 {
    private function __construct() {
    }

    /**
     * UTF-8 HTML5-compatible Tidy
     *
     * @param string $html
     * @param array $config
     * @param string $encoding
     * @link http://tidy.sourceforge.net/docs/quickref.html
     * @see https://www.php.net/manual/en/tidy.cleanrepair.php#123010 // modified
     */
    public static function tidy(string $html, array $config = [], $encoding = 'utf8'): string {
        $config += [
            'clean' => false,
            'doctype' => 'html5',
            'join-styles' => false,
            'new-blocklevel-tags' => 'article aside audio bdi canvas details dialog figcaption figure footer header hgroup main menu menuitem nav section source summary template track video',
            'new-empty-tags' => 'command embed keygen source track wbr',
            'new-inline-tags' => 'audio command datalist embed keygen mark menuitem meter output progress source time video wbr',
            'output-html' => true,
            'tidy-mark' => false,
            'wrap' => 0,
        ];

        $html = tidy_parse_string('<meta charset="utf-8">' . $html, $config, $encoding);
        tidy_clean_repair($html); // replaces the tidy object with a string

        $dom = new \DOMDocument(encoding: 'utf-8');
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        @$dom->loadHtml((string) $html, LIBXML_HTML_NODEFDTD | LIBXML_HTML_NOIMPLIED | LIBXML_NOBLANKS); // this cast is to make intellisense happy
        $body = $dom->getElementsByTagName('body')->item(0);

        $output = '';
        // foreach: convert every child of $body to an HTML string and append it to the $output string
        foreach (iterator_to_array($body->childNodes) as $node) { // iterator_to_array: get all of the direct children of $body as an array
            $output .= $body->ownerDocument->saveHTML($node);
        }

        return preg_replace('/[\s\n]+/u', ' ', $output); // preg_replace: normalize whitespace
    }
}
