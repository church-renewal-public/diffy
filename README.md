# Diffy

A small collection of fast, easy-to-use classes to compute and display differences between things in various formats.

# Installation

```bash
composer require church-renewal/diffy
```

Requirements other than those specified in `composer.json` are:

- Read and write permission to `/tmp`
- GNU `diff` version 3.\*

# Usage

All abstract classes, classes, and exceptions are in the ChurchRenewal\Diffy namespace.

## Examples

**GnuDiff**

Get an array of associative arrays with the keys "start", "end", and "delta".  
| element | description |
| --- | --- |
| `delta` | `int` — the count of elements in the input array that are involved in the change. Negative numbers indicate a deletion and that `start` references `$oldStrings`, and positive numbers indicate an insertion and that `start` references `$newStrings`. This will never be zero |
| `start` | `int` — index in the input array that the change starts on |
| `end` | `int` — `start` + abs(`delta`) |

```php
$diff = ChurchRenewal\Diffy\GnuDiff::getDiff(
    ["Hello", "world"],
    ["Hello", "everyone"],
);
echo json_encode($diff);
```

```json
/* prettied for legibility */
[
    {"start": 1, "end": 2, "delta": -1},
    {"start": 1, "end": 2, "delta":  1}
]
```

**StyleInsensitiveDiff**

```php
$diff = new ChurchRenewal\Diffy\StyleInsensitiveDiff(
    '<p><i>Hello, world!</i></p>',
    '<p>Hello everyone!</p>',
);
echo $diff->getDiff();
```

```html
<p>Hello<i><del class="diffdel">,</del></i> <i><del class="diffdel">world!</del></i><ins class="diffins">everyone!</ins></p>

<!-- again, but prettied for legibility -->
<p>
    Hello
    <i><del class="diffdel">,</del></i>
    <i><del class="diffdel">world!</del></i>
    <ins class="diffins">everyone!</ins>
</p>
```
**StyleSensitiveDiff**

```php
$diff = new ChurchRenewal\Diffy\StyleSensitiveDiff(
    '<p><i>Hello, world!</i></p>',
    '<p>Hello everyone!</p>',
);
echo $diff->getDiff();
```

```html
<p><i><del class="diffdel">Hello, world!</del></i><ins class="diffins">Hello everyone!</ins></p>

<!-- again, but prettied for legibility -->
<p>
    <i><del class="diffdel">Hello, world!</del></i>
    <ins class="diffins">Hello everyone!</ins>
</p>
```

# Running Tests

```bash
composer install --dev
vendor/bin/phpunit

# if you want a code coverage report
XDEBUG_MODE=coverage vendor/bin/phpunit --coverage-html test-coverage --coverage-filter src
```
